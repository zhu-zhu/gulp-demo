const { 
  src, // 创建一个流，用于从文件系统读取文件数据
  dest, // 创建一个用于将文件数据写入到文件系统的流
  parallel, // 将任务组合，并同时执行
  series, // 将任务组合，并顺序执行
  watch // 监听文件变化
} = require('gulp')
const loadPlugins = require('gulp-load-plugins')
const browserSync = require('browser-sync')
const del = require('del')

const plugins = loadPlugins()
const bs = browserSync.create()

const data = require('./pageData')

// scss 编译
const style = () => {
  return src('src/assets/styles/*.scss', { base: 'src' })
    .pipe(plugins.sass({ outputStyle: 'expanded' }))
    .pipe(dest('temp'))
    .pipe(bs.reload( { stream: true } ))
}

// js 编译
const script = () => {
  return src('src/assets/scripts/*.js', { base: 'src' })
    .pipe(plugins.babel({ presets: ['@babel/preset-env'] }))
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

// 编译 swig 模板
const page = () => {
  return src('src/*.html', { base: 'src' })
    // swigJson data为注入的数据，defaults 取消缓存
    .pipe(plugins.swig({ data, defaults: { cache: false } }))
    .pipe(dest('temp'))
    .pipe(bs.reload( { stream: true } ))
}

// 压缩图片
const image = () => {
  return src('src/assets/images/**', { base: 'src' })
    .pipe(plugins.imagemin())
    .pipe(dest('dist'))
}

// 压缩文字中引用的图片
const font = () => {
  return src('src/assets/fonts/**', { base: 'src' })
    // 有些字体会关联 svg 文件 所以也要使用图片压缩
    .pipe(plugins.imagemin())
    .pipe(dest('dist'))
}

// 其他文件 如 icon
const extra = () => {
  return src('public/**', { base: 'public' })
    .pipe(dest('dist'))
}

const serve = () => {
  // 第一个参数为监视的文件，第二个为执行的任务
  watch('src/assets/styles/*.scss', style)
  watch('src/assets/scripts/*.js', script)
  watch('src/*.html', page)
  watch([
    'src/assets/images/**',
    'src/assets/fonts/**',
    'public/**',
  ], bs.reload)

  bs.init({
    // routes 会优先于 baseDir，发生请求会先查看 routes 再看 baseDir
    // routes key 为请求的地址，value 为指向的目录
    notify: false, // 取消弹出 bs 的提示框
    port: 2080, // 端口
    open: false, // 取消自动打开浏览器
    server: {
      baseDir: ['temp', 'src', 'public'],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  })
}

const useref = () => {
  return src('temp/*.html', { base: 'temp' })
    // 传入参数 searchPath 需要合并的文件的目录
    .pipe(plugins.useref( { searchPath: ['temp', '.'] } ))
    // 判断是否是 .js 的文件，是的话执行相应的任务
    .pipe(plugins.if(/\.js$/, plugins.uglify()))
    .pipe(plugins.if(/\.css$/, plugins.cleanCss()))
    .pipe(plugins.if(/\.html$/, plugins.htmlmin({
      collapseWhitespace: true, // 删除换行空格符
      minifyCSS: true, // 压缩行内样式
      minifyJS: true // 压缩行内js
    })))
    .pipe(dest('dist'))
}

const clean = () => {
  // 传入要删除的数组
  // del 返回的是一个 Promise 方法，所以可以直接使用，gulp会根据返回的结果判定任务是否完成
  return del(['dist', 'temp'])
}

const compile = parallel(style, script, page)
const build = series(clean, parallel(series(compile, useref), image, font, extra))
const devlop = series(compile, serve)

module.exports = {
  compile,
  clean,
  build,
  devlop
}
