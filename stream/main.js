const { createReadStream, createWriteStream } = require('fs')
const { Transform } = require('stream')
const { renderSync } = require('node-sass')

// 文件读取流
const read = createReadStream('demo.scss')
// 文件写入流
const write = createWriteStream('demo.min.css')

const scssToCss = () => {
  return new Transform({
    transform(chunk, encodeing, callback) {
      // chunk => 流返回的字符编码
      const input = chunk.toString()
      const output = renderSync({
        data: input,
        outputStyle: 'expanded'
      })
      callback(null, output.css)
    }
  })
}

const minCss = () => {
  return new Transform({
    transform(chunk, encodeing, callback) {
      const input = chunk.toString()
      const output = input.replace(/\n/g, '')
      callback(null, output)
    }
  })
}

read
  .pipe(scssToCss()) // 转换流
  .pipe(minCss())	// 转换流
  .pipe(write)
